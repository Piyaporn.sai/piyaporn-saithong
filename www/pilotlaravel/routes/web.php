<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $user=new \App\User();
    $user->setAttribute('name','joe');
    $user->setAttribute('email','joe@msu.ac.th');
    $user->setAttribute('password',Hash::make('12345'));
    $user->save();

    return 'the user is created';

    //return "hello world";
});
